package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> prices = new HashMap<>();


    double getBookPrice(String isbn) {
        prices.put("1", 10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
        return prices.get(isbn);
    }

    double getFahrenheit(String celsius) {
        return (Integer.parseInt(celsius) * 1.8) + 32;
    }


}
